Requisits Mínims </br>
- Personatge amb controls de teclat </br>
- Moviments per físiques </br>
- Implementació de col·lisions </br>
- Condició de derrota amb GameOver </br>
- Instanciació i Destrucció d’objectes per codi </br>
- Control temporal mitjançant funcions com Invoke i InvokeRepeating </br>
- Concepte d’Aleatorietat </br>
- Puntuació mostrada per pantalla mitjançant canvas </br>
- Moviment de la càmara. </br>
</br></br>

-Requisits Addicionals</br>
- Augment progressiu de la dificultat.</br>
- Accés a components per codi més enllà del RigidBody i el Transform</br>
- Parallax (Poster Kirito Vampiro)</br>
- Canvas amb GameOver y puntuació</br>
- Botó que reinicia la partida al Canvas GameOver</br>



