﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class gameover : MonoBehaviour
{

    public GameObject gameOverCanvas;
    public Text textScoreCanvas;
    private mov player;

    void Start()
    {
        //Llamamos a player del script mov
        player = FindObjectOfType<mov>();

        // Desactivamos el Canvas gameOver, just in case.
        gameOverCanvas.SetActive(false);
    }

    void Update()
    {
        //Miramos si el jugador esta vivo para poder hacer el gameOver
        if (!player.vivo) {
            gameOverCanvas.SetActive(true);
            textScoreCanvas.text = FindObjectOfType<map>().score.text;
        }
    }

    //Uso para el boton del GameOver para reiniciar la partida
    public void loadLevel(int nivel)
    {
        SceneManager.LoadScene(nivel);
    }
}
