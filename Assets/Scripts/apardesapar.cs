﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class apardesapar : MonoBehaviour
{
    mov player;
    public float dist = 9;


    // Start is called before the first frame update
    void Start()
    {
        //Llamamos a player del script mov
        player = FindObjectOfType<mov>();
    }

    // Update is called once per frame
    void Update()
    {
        //Si player no es null = no esta muerto
        if (player != null)
        {
            //Movemos las plataformas para hacerlo infinito
            if (player.transform.position.x - this.transform.position.x >= this.transform.localScale.x * 16)
            {
                this.transform.position = new Vector3(
                   this.transform.position.x + this.transform.localScale.x * dist, this.transform.position.y, this.transform.position.z);

            }
        }
    }
}
