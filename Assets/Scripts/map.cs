﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class map : MonoBehaviour
{
    //FollowPlayer player;

    public Text score;
    public float puntos = 0;

    public GameObject[] blockPrefabs;
    public float anchoobs = 2.0f;
    public float disobs;
    public bool ganapuntos = true;

    public GameObject cosoFondo;

    private GameObject player;


    void Start()
    {
        //hacemos que player sea mov (Donde tenemos todo lo del PJ)
        player = FindObjectOfType<mov>().gameObject;

        //Hacemos el InvokeRepeating porque hay posters de Tokyo Ghoul que queremos 
        //que salgan cada 5s y al principio spawnea al segundo porque si, porque mola
        //porque es la moda, Tokyo Ghoul es caca pero bueno, cosas de Otakus Mugrosos
        InvokeRepeating("creaCosa", 1f, 5f);

        //la distancia del objeto es igual al ancho del objeto * 9.8
        disobs = anchoobs * 9.8f;
        //anchoobs = blockPrefabs[0].transform.localScale.x;

        //Iniciamos el primer bloque porque queremos que el primero siempre sea el mismo
        GameObject newBlock = Instantiate(blockPrefabs[5]);
        newBlock.transform.localScale = new Vector2(anchoobs, newBlock.transform.localScale.y);
        newBlock.transform.position = new Vector2(0, -4f);

        //Iniciamos los demas bloques
        for (int i = 1; i < blockPrefabs.Length; i++) {
            
            newBlock = Instantiate(blockPrefabs[Random.Range(0, blockPrefabs.Length)]);
            newBlock.transform.localScale = new Vector2(anchoobs, newBlock.transform.localScale.y);
            newBlock.transform.position = new Vector2((newBlock.transform.localScale.x * disobs)* i, -4f);
            

        }

        //Actualizamos el color y tamaño de score
        score.color = Color.black;
        score.fontSize = 300;

    }

    void Update()
    {
        //Si no esta muerto y esta con opciones de ganar puntos
        if (ganapuntos)
        {
            //Suma puntos y lo actualizamos
            puntos += 1;
            score.text = "Score: " + this.puntos;
        }

    }

    void creaCosa()
    {
        //Si player no esta muerto
        if (player != null)
        {
            //Iniciamos el poster de Tokyo Ghoul, ese anime de Vampiros o algo asi, no me acuerdo mucho
            GameObject coso = (GameObject)Instantiate(cosoFondo, player.transform);
            coso.transform.position = new Vector2(player.transform.position.x + 10, transform.position.y + 4);
            coso.transform.parent = transform.parent;
            //Parallax un poco trampas en los posters de Kirito Vampiro
            coso.GetComponent<Rigidbody2D>().velocity = new Vector2(100f * Time.deltaTime, 0);
            //Cuando pasan 5 segundos lo borramos
            Destroy(coso, 7f);

        }
    }

}
