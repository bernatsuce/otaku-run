﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FollowPlayer : MonoBehaviour
{
    
    private Transform player;
    public float offsety;
  

    private void Start()
    {
        //Busca player (mov) y como solo necesitamos transform lo pasamos a transform
        player = FindObjectOfType<mov>().transform; 
    }

    // Update is called once per frame
    void Update()
    {
        //Si el player esta vivo, la camara sigue el player con el script mov
        if (player != null)
        {
            this.transform.position = new Vector3(
                player.transform.position.x,
                player.transform.position.y + offsety,
                this.transform.position.z);
           
        }

        
    }
}
