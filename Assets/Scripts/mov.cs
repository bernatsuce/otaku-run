﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class mov : MonoBehaviour
{
    public float speed = 2;
    private bool salto = true;
    private Rigidbody2D rb;
    private GameObject player;
    public bool vivo = true;

    // Start is called before the first frame update
    void Start()
    {
        //Si player esta muerto busca por el tag player
        if (player == null)
        {
            player = GameObject.FindWithTag("Player");
        }

        //Para ahorrar codigo y no escribir siempre Rigdbody2d hacemos el rb =
        rb = this.GetComponent<Rigidbody2D>();
        //Decimos que el player esta vivo, por si acaso
        vivo = true;
    }

    // Update is called once per frame
    void Update()
    {
        //Si esta vivo
        if (vivo)
        {
            //aumentamos la velocidad progresivamente
            speed += 0.1f * Time.deltaTime;

            //Si se presiona el espacio y salto es true
            if (Input.GetKeyDown("space") && salto)
            {
                //Añadimos la fuerza a rb
                rb.AddForce(new Vector2(0, 300));
                //Y decimos que el salto es false para no permapulsar el espacio y el personaje salga volando
                salto = false;
            }

            //Asigna la velocidad horizontal al personaje y lo mantiene en la Y
            rb.velocity = new Vector2(speed, rb.velocity.y);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        //Si colisiona con el tag suelo
        if (collision.gameObject.tag == "suelo")
        {
            //OLE MI BETIIIIIIIIS (Solo para comprobar)
            print("ole betis");
            //Decimos que se puede saltar
            salto = true;
        }
       
        //Si colisiona con obs (Obstaculo)
        if (collision.gameObject.tag == "obs")
        {
            //Buscamos map y lo guardamos como m
            map m = FindObjectOfType<map>();
            //Hacemos que pare de ganar puntos
            m.ganapuntos = false;
            //El personaje muere
            vivo = false;
            //Y destruimos el personaje
            GameObject.Destroy(this.gameObject);
        }



    }
}